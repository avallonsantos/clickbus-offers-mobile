// Importing global sass

import './assets/scss/app.scss';

/**
 * Importing route offers
 */

import routes from './routes.csv';

import CardOffersList from "./components/card-list";
import CreateAccordions from "./components/accordion";
import CreateCarousel from "./components/carousels";
import CBWidget from "./components/widget";

document.addEventListener('DOMContentLoaded', () => {
    const Cards = new CardOffersList('#list-card-inline-offers', routes);
    Cards.createList();
    CreateAccordions();
    CreateCarousel();
    CBWidget.init();
});