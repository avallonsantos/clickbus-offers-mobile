/**
 * Importing required dependencies
 */
import currency from 'currency.js';

export default (value) => currency(value, {
    formatWithSymbol: false,
    decimal: ',',
    separator: '.'
}).format();