const CBWidget = new ClickBusWidget({
    orientation: 'horizontal',
    maxSize: 1500,
    theme: 'white'
});

export default CBWidget;