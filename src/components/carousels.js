import Glide from '@glidejs/glide';

// Function to update Peek Options

const UpdatePeekValues = (carousel, beforeValue, afterValue) => {
    carousel.update({
        peek: {
            before: beforeValue,
            after: afterValue
        }
    })
}

const UpdatePeekBasedOnSlideScroll = carousel => {
    carousel.on('run.before', (opt) => {
        let index = carousel.index + 1, slides = carousel._c.Sizes.length
        if ((slides - index === 2 || (slides - index === 1)) && opt.direction === '>') {
            UpdatePeekValues(carousel, 80, 24)
        } else {
            UpdatePeekValues(carousel, 24, 80)
        }
    })
}

const CreateCarousel = () => {
    let carouselOffers = new Glide('#card-routes-list', {
        type: 'slide',
        perView: 1,
        gap: 24,
        peek: {
            before: 24,
            after: 80
        }
    });

    UpdatePeekBasedOnSlideScroll(carouselOffers);

    carouselOffers.mount();
};

export default CreateCarousel;