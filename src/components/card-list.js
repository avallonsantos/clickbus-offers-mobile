/**
 * Importing dependencies
 */
import formatCurrency from './helpers/format-currency';

class CardOffersList {
    constructor(target, routes) {
        this.target = document.querySelector(target);
        this.routes = routes;
    }

    _changeFinalUrlDate() {
        this.target.querySelectorAll('.available-dates').forEach(date => {
            date.addEventListener('change', function () {
                let link = date.parentNode.nextElementSibling.getAttribute('href').split('=')[0] + '=' + date.value;
                date.parentNode.nextElementSibling.setAttribute('href', link);
            })
        });
    }

    _filterTable() {
        let triggerFilter = document.querySelectorAll('.filter-trigger'),
            cards = this.target.querySelectorAll('.card-offer');

        triggerFilter.forEach(function (triggerElement) {
            triggerElement.addEventListener('change', function () {
                let triggerOriginValue = document.getElementById('origin-offer').value;
                let triggerDestinationValue = document.getElementById('destination-offer').value;
                let trigger = this;

                if (triggerOriginValue !== '' && triggerDestinationValue === '') {
                    return cards.forEach(card => {
                        console.log(card.getAttribute('data-origin-offer'));
                        if (triggerOriginValue === card.getAttribute('data-origin-offer')) {
                            return card.style.display = 'block';
                        }
                        card.style.display = 'none';
                    });
                }

                if (triggerOriginValue === '' && triggerDestinationValue !== '') {
                    return cards.forEach(card => {
                        if (triggerDestinationValue === card.getAttribute('data-destination-offer')) {
                            return card.style.display = 'block';
                        }
                        card.style.display = 'none';
                    });
                }

                if (triggerOriginValue !== '' && triggerDestinationValue !== '') {
                    return cards.forEach(card => {
                        if (triggerOriginValue === card.getAttribute('data-origin-offer') && triggerDestinationValue === card.getAttribute('data-destination-offer')) {
                            return card.style.display = 'block';
                        }
                        card.style.display = 'none';
                    });
                }

                cards.forEach(card => card.style.display = 'block');
            });
        });
    }

    _expandOptions() {
        this.target.querySelectorAll('.card-offer').forEach(card => {
            card.addEventListener('click', () => {
                card.classList.toggle('opened');
            });
        });

        this.target.querySelectorAll('.offer-options').forEach(option => {
            option.addEventListener('click', e => e.stopImmediatePropagation());
        })
    }

    createList() {
        const insertedOrigins = [];
        const insertedDestinations = [];
        this.routes.forEach(route => {
            let availables = route.availableDates.split(',');

            let itemList = `
                <div class="card-offer" data-origin-offer="${route.originSlug}" data-destination-offer="${route.destinationSlug}">
                    <i class="icon-chevron-down"></i>
                    <div class="card-offer-summary d-flex flex-align-items-center">
                        <div class="route-offer">
                            <span><i class="icon-origin"></i> ${route.origin}</span>
                            <span><i class="icon-destination"></i> ${route.destination}</span>
                        </div>
                        <!-- /.route-offer -->
                        <div class="price-offer">
                            <span class="old-price">${formatCurrency(parseFloat(route.referencePrice))}</span>
                            <span class="current-price">${formatCurrency(parseFloat(route.price))}</span>
                        </div>
                        <!-- /.price-offer -->
                    </div>
                    <!-- /.card-offer-summary d-flex flex-align-items-center -->

                    <div class="offer-options">
                        <label>Datas Disponíveis</label>
                        <div class="content d-flex flex-align-items-center">
                            <div class="custom-select-box input-field">
                                <span class="icon icon-sort-desc icon-document"></span>
                                <select class="available-dates">
                                     ${availables.map(data => {
                let dateSplited = data.trim().split('-');
                return `<option value="${data.trim()}">${dateSplited[2]}/${dateSplited[1]}/${dateSplited[0]}</option>`
            }).join('')}
                                </select>
                                <!-- /.custom-select-box input-field -->
                            </div> <!-- / .custom-select-box -->

                            <a target="_blank" href="https://www.clickbus.com.br/onibus/${route.originSlug}/${route.destinationSlug}?departureDate=${availables[0]}">Pesquisar</a>
                        </div>
                        <!-- /.content d-flex flex-align-items-center -->
                    </div>
                    <!-- /.offer-options -->
                </div>
                <!-- /.card-offer -->
            `;

            if (!insertedOrigins.includes(route.originSlug)) {
                document.getElementById('origin-offer').insertAdjacentHTML('beforeend', `<option value="${route.originSlug}">${route.origin}</option>`);
            }

            if (!insertedDestinations.includes(route.destinationSlug)) {
                document.getElementById('destination-offer').insertAdjacentHTML('beforeend', `<option value="${route.destinationSlug}">${route.destination}</option>`);
            }

            insertedOrigins.push(route.originSlug);
            insertedDestinations.push(route.destinationSlug);

            this.target.insertAdjacentHTML('beforeend', itemList);
        });

        this._expandOptions();
        this._changeFinalUrlDate();
        this._filterTable();
    }
}

export default CardOffersList;