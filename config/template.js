const path = require('path')
// Importing meta tags for this landing page
const Metas = require('./metas');

// Exporting literal object with configuration of HTML Webpack Plugin

module.exports = {
    links: [],
    scripts: [{
        src: 'https://static.clickbus.com/live/ClickBus/frontend-widget/js/clickbus-widget.min.js'
    }],
    title: 'Teste de título dessa bagaça',
    mobile: true,
    lang: 'pt_BR',
    template: path.resolve(__dirname, './../src/static/index.html'),
    meta: Metas,
    renderPlaceholders: false
};